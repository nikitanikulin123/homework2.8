var MongoClient = require('mongodb').MongoClient
	, format = require('util').format;
var url = 'mongodb://127.0.0.1:27017/homework8';

MongoClient.connect(url, function(err, db) {
	if(err) {
		console.log('Невозможно подключиться к БД. Ошибка:', err)
	} else {
		console.log('Соединение установлено для', url)
	}

	var collection = db.collection('cars');

	var car1 = {name: 'Zaporozhec', price: 1000};
	var car2 = {name: 'Lada', price: 5000};
	var car3 = {name: 'Mustang', price: 20000};

	collection.insert([car1, car2, car3], function(err, docs) {
		if(err) {
			console.log(err);
		} else {
			collection.find({name: {$regex: /a/}}).toArray(function (err, results) {
				if (err) {
					console.log(err);
				} else if (results.length) {
					console.log('Найденный:', results);
				} else {
					console.log('Нет документов с данным условием поиска');
				}
			});
			collection.remove();
		}
		db.close();
	});
});